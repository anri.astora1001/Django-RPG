from django.db import models
from django.shortcuts import get_object_or_404
from PIL import Image
from .inventory import *


def process_item_action(item_id, player):
    item = get_object_or_404(Item, id = item_id)
    action = item.data.action
    if action == "equip":
        if item.is_equiped:
            item.is_equiped = False
            item.save()
            parse_unequip(item, player)
        else:
            parse_equip_action(item, player)
    elif action == "use":
        parse_use_action(item, player)

def npc_nearby(player):
    npc_list = []
    if(not player.location.north == None and len(player.location.north.npcs.all()) >= 1):
        for npc in player.location.north.npcs.all():
            if  npc.name_known:
                npc_list.append([npc.name, npc.id])
            else:
                npc_list.append([npc.race, npc.id])
        return True, npc_list
    elif not player.location.north == None and len(player.location.south.npcs.all()) >= 1:
        for npc in player.location.south.npcs.all():
            if  npc.name_known:
                npc_list.append([npc.name, npc.id])
            else:
                npc_list.append([npc.race, npc.id])
        return True, npc_list
    return False, npc_list


def compile_actions(player_id):
    player = get_object_or_404(Player, id = player_id)
    actions = []
    dialogue_actions = []
    npc_nearby_bool, npc_list = npc_nearby(player)
    if(len(player.location.items.all()) > 0 ):
        actions.append("search")
    if(npc_nearby_bool):
        for npc in npc_list:
            dialogue_actions.append([f"talk to {npc[0]}", npc[1]])
        
    return actions, dialogue_actions

#functions that are in both view and model go here to prevent circular import
def render_objects(node, current_tile):
    try:
        player = get_object_or_404(Player, id = 3)
        if(player.location.current.name == node.current.name):
                player_icon = Image.open("media/objects/player.png")
                #the image you need to paste on
                current_tile.paste(player_icon, (0,0), player_icon)
                #current_tile = Image.alpha_composite(current_tile, player_icon)
    except:
        pass
    try:
        items = node.items
        for item in items:
            item_icon = Image.open(item.icon.path)
            current_tile.paste(item_icon, (0,0), item_icon)
    except:
        pass


class Action(models.Model):
    name = models.CharField(max_length=100)

class Tile(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(null = True, blank = True, upload_to="tiles/")

    is_walkable = models.BooleanField(blank = True, default=True)

    is_climbable = models.BooleanField(blank = True, default=False)

    def __str__(self):
        return self.name


class Node(models.Model):
    name = models.CharField(max_length=100)

    description = models.TextField()

    tile = models.ForeignKey(Tile, null = True, blank=True, on_delete = models.CASCADE, related_name='tile')
    
    def __str__(self):
        return self.name
 

class Map(models.Model):
    name = models.CharField(max_length=100)

    #I don't need this I can just save it as file remotely.
    image = models.ImageField(null = True, blank = True, upload_to="maps/")

    description = models.TextField(null = True)

    is_loaded = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    @classmethod
    def upload_image(self, path, map_id):
        map = get_object_or_404(Map, id = map_id)

        map.image = path
        map.save()

        image = Image.open(map.image.path)
        image.save("media/maps/map.jpg")

    

class MapNode(models.Model):
    current = models.ForeignKey(Node, null = True, blank = True, on_delete = models.CASCADE, related_name='current')


    region = models.ForeignKey(Map, blank = True, on_delete = models.CASCADE, related_name= "map")

    north = models.ForeignKey('self', null = True, blank=True, on_delete = models.CASCADE, related_name='north_node')
    south = models.ForeignKey('self', null = True, blank=True, on_delete = models.CASCADE, related_name='south_node')
    east = models.ForeignKey('self', null = True, blank=True, on_delete = models.CASCADE, related_name='east_node')
    west = models.ForeignKey('self', null = True, blank=True, on_delete = models.CASCADE, related_name='west_node')

    def __str__(self):
        return self.current.name
    
    @classmethod
    def merge_east(self, node_id):
        node = get_object_or_404(MapNode, id = node_id)
        current_node = Image.open(node.current.tile.image.path) 
        current_row = Image.open("media/maps/current_row.jpg")

        #render_objects(node, current_node)

        merged = Image.new("RGB", (current_row.width + current_node.width, current_row.height))
        merged.paste(current_row, (0,0))
        merged.paste(current_node, (current_row.width, 0))
        merged.save("media/maps/current_row.jpg")

    @classmethod
    def merge_west(self, node_id):
        node = get_object_or_404(MapNode, id = node_id)
        current_node = Image.open(node.current.tile.image.path) 
        current_row = Image.open("media/maps/current_row.jpg")

        #render_objects(node, current_node)

        merged = Image.new("RGB", (current_row.width + current_node.width, current_row.height))
        merged.paste(current_node, (0,0))
        merged.paste(current_row, (current_node.width, 0))
        merged.save("media/maps/current_row.jpg")
    
        
    

class MapStart(models.Model):
    region = models.OneToOneField(Map, null= True, blank= True, on_delete=models.CASCADE, related_name="map_start")
    start_node = models.ForeignKey(MapNode, null= True, blank = True, on_delete=models.CASCADE, related_name="start") 

class Door(models.Model):
    node1 = models.ForeignKey(MapNode, null = True, blank = True, on_delete=models.CASCADE, related_name="door1")
    node2 = models.ForeignKey(MapNode, null = True, blank = True, on_delete=models.CASCADE, related_name="door2")


class Inventory(models.Model):

    gold = models.IntegerField(default=0)
   
    def __str__(self):
        try:
            self.player.name
            return self.player.name
        except:
            return "unowned"

class GameItem(models.Model):
    name = models.CharField(max_length=100)
    icon = models.ImageField(null = True, blank = True, upload_to="icons/")
    description = models.TextField(null=True, blank = True)

    action = models.CharField(max_length=100, null = True)
    action_details = models.TextField(null=True)

    def __str__(self):
        return self.name

class Item(models.Model):
    data = models.ForeignKey(GameItem, null = True, blank = True, on_delete=models.CASCADE, related_name="info")

    quantity = models.PositiveIntegerField(default=1)

    location = models.ForeignKey(MapNode, null = True, blank = True, on_delete=models.CASCADE, related_name='items')
    
    inventory = models.ForeignKey(Inventory, null = True, blank = True, on_delete=models.CASCADE, related_name="items")

    is_equiped = models.BooleanField(default=False)

    def __str__(self):
        return self.data.name


class ArmorSlot(models.Model):
    head = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="head")
    torso = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="torso")
    legs = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="legs")
    feet = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="feet")
    hands = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="hands")

    inventory = models.ForeignKey(Inventory, null = True, blank = True, on_delete=models.CASCADE, related_name="armor_slots")

    def __str__(self):
        try:
            self.inventory.player.name
            return self.inventory.player.name
        except:
            return "unowned"

class Greeting(models.Model):
    name = models.CharField(max_length=100)
    tags = models.TextField(null = True, blank = True)
    action_tags = models.TextField(null = True, blank = True)
    text = models.TextField()

class Farewell(models.Model):
    name = models.CharField(max_length=100)
    tags = models.TextField(null = True, blank = True)
    action_tags = models.TextField(null = True, blank = True)
    text = models.TextField()
    
class Joke(models.Model):
    name = models.CharField(max_length=100)
    tags = models.TextField(null = True, blank = True)
    action_tags = models.TextField(null = True, blank = True)
    text = models.TextField()

class Romance(models.Model):
    name = models.CharField(max_length=100)
    tags = models.TextField(null = True, blank = True)
    action_tags = models.TextField(null = True, blank = True)
    text = models.TextField()

class MainText(models.Model):
    current = models.TextField(null = True)
    prev_1 = models.TextField(null = True)

class PowerWord(models.Model):
    word = models.CharField(max_length=100)
    tags = models.TextField(null = True, blank = True)
    incompatible_tags = models.TextField(null = True, blank = True)
    action_commands = models.TextField(null = True, blank = True)
    text = models.TextField()

    def __str__(self):
        return self.word


class Race(models.Model):
    name = models.CharField(max_length=100)
    tags = models.TextField(null = True)
    min_height = models.IntegerField(null = True)
    max_height = models.IntegerField(null = True)

class NPC(models.Model):
    name = models.CharField(max_length=100)
    race = models.CharField(max_length=100, null = True)

    location = models.ForeignKey(MapNode, blank=True, on_delete = models.CASCADE, related_name="npcs")

    is_merchant = models.BooleanField(blank=True, default=False)
    name_known = models.BooleanField(blank=True, default=False)
    spoken_to_recently = models.BooleanField(blank = True, default= True)
    greeted_recently = models.BooleanField(default=False)
    



class Player(models.Model):
    name = models.CharField(max_length=100, blank = True)

    location = models.ForeignKey(MapNode, null = True, blank=True, on_delete = models.PROTECT, related_name="player")

    inventory = models.OneToOneField(Inventory, null = True, blank = True, on_delete= models.CASCADE, related_name="player")

    armor = models.ForeignKey(ArmorSlot, null = True, blank = True, on_delete=models.CASCADE, related_name="player")

    main = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_main")
    offhand = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_offhand")

    known_power_words = models.ManyToManyField(PowerWord, blank = True, default = None)

    charisma = models.IntegerField(null = True, blank = True, default=0)

    prowess = models.IntegerField(null = True, blank = True, default=0)
    effusion = models.IntegerField(null = True, blank = True, default=0)
    constitution = models.IntegerField(null = True, blank = True, default=0)
    understanding = models.IntegerField(null = True, blank = True, default=0)
    luck = models.IntegerField(null = True, blank = True, default=0)
    insight = models.IntegerField(null = True, blank = True, default=0)
    agility = models.IntegerField(null = True, blank = True, default=0)
    rapture = models.IntegerField(null = True, blank = True, default=0)

    hat = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_hat")
    spectacles = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_spectacles")
    mask = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_mask")
    shirt = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_shirt")
    coat = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_coat")
    cloak = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_cloak")
    gloves = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_gloves")
    belt = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_belt")
    pants = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_pants")
    socks = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_socks")

    talisman1 = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_talisman1")
    talisman2 = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_talisman2")
    talisman3 = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_talisman3")
    talisman4 = models.ForeignKey(Item, null = True, blank = True, on_delete=models.CASCADE, related_name="player_talisman4")

    dialogue_actions = models.TextField(null = True, blank = True, default=None)
    last_dialogue_actions = models.TextField(null = True, blank = True, default=None)
    current_dialogue_options = models.CharField(max_length=100, null = True, blank = True, default=None)
    last_farewell = models.ForeignKey(Farewell, null = True, blank = True, default=None, on_delete=models.CASCADE)
    def __str__(self):
        return self.name

    @classmethod
    def movement(self, player, direction):
        if(direction == "down"):
            if(player.location.south == None or not player.location.south.current.tile.is_walkable and not len(player.location.south.npcs) >= 1):
                if(player.location.south.current.tile.is_climbable and 
                    (not player.location.south.south == None and player.location.south.south.current.tile.is_walkable) and 
                    not len(player.location.north.north.npcs) >= 1):
                    player.location = player.location.south.south
                    player.save()
                return
                
            player.location = player.location.south
            player.save()
            #print(player.location.current.name)
            return
        elif(direction == "up"):
            if(player.location.north == None or not player.location.north.current.tile.is_walkable and not len(player.location.north.npcs) >= 1):
                if(player.location.north.current.tile.is_climbable and 
                    (not player.location.north.north == None and player.location.north.north.current.tile.is_walkable) and 
                    not len(player.location.north.north.npcs) >= 1):
                    player.location = player.location.north.north
                    player.save()
                return
            player.location = player.location.north
            player.save()
            return
        elif(direction == "right"):
            if(player.location.east == None or not player.location.east.current.tile.is_walkable):
                if(player.location.east.current.tile.is_climbable and 
                    (not player.location.east.east == None and player.location.east.east.current.tile.is_walkable)):
                    player.location = player.location.east.east
                    player.save()
                return
            player.location = player.location.east
            player.save()
            return
        elif(direction == "left"):
            if(player.location.west == None or not player.location.west.current.tile.is_walkable):
                if(player.location.west.current.tile.is_climbable and 
                    (not player.location.west.west == None and player.location.west.west.current.tile.is_walkable)):
                    player.location = player.location.west.west
                    player.save()
                return
            player.location = player.location.west
            player.save()
            return
        


class SavedSpell(models.Model):
    name = models.CharField(max_length=100)

    main_word = models.ForeignKey(PowerWord, null = True, blank=True, on_delete = models.CASCADE, related_name="mains")
    mod_word1 = models.ForeignKey(PowerWord, null = True, blank=True, on_delete = models.CASCADE, related_name="mod1s")
    mod_word2 = models.ForeignKey(PowerWord, null = True, blank=True, on_delete = models.CASCADE, related_name="mod2s")
    mod_word3 = models.ForeignKey(PowerWord, null = True, blank=True, on_delete = models.CASCADE, related_name="mod3s")
    mod_word4 = models.ForeignKey(PowerWord, null = True, blank=True, on_delete = models.CASCADE, related_name="mod4s")

    player = models.ForeignKey(Player, null = True, blank=True, on_delete = models.CASCADE, related_name="spells")

class GameState(models.Model):
    name = models.CharField(max_length=100)
    location =models.ForeignKey(MapNode, blank=True, on_delete = models.CASCADE)

    actions = models.ManyToManyField(Action, blank=True)
