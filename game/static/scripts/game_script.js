//event listener
window.addEventListener("keydown", onKeyDown, false);
window.addEventListener("keyup", onKeyUp, false);

var keyW = false;
var keyA = false;
var keyS = false;
var keyD = false;

function onKeyDown(event) {
  var keyCode = event.keyCode;
  switch (keyCode) {
    case 68: //d
      keyD = true;
      send_direction_ajax("right");
      console.log("pressed d");
      break;
    case 83: //s
      keyS = true;
      send_direction_ajax("down");
      break;
    case 65: //a
      keyA = true;
      send_direction_ajax("left");
      break;
    case 87: //w
      keyW = true;
      send_direction_ajax("up");
      break;
  }
}

function onKeyUp(event) {
  var keyCode = event.keyCode;

  switch (keyCode) {
    case 68: //d
      keyD = false;
      break;
    case 83: //s
      keyS = false;
      break;
    case 65: //a
      keyA = false;
      break;
    case 87: //w
      keyW = false;
      break;
  }
}

function send_direction_ajax(direction){
    $.ajax({
        url: "game/movement/",
        type: "POST",
        dataType: "json",
        data: JSON.stringify({payload: direction,}),
        headers: {
          "X-Requested-With": "XMLHttpRequest",
          "X-CSRFToken": getCookie("csrftoken"),  // don't forget to include the 'getCookie' function
        },
        success: (data) => {
            console.log("sent direction to the view")
          console.log(data);
        },
        error: (error) => {
            console.log("didn't send it to the view")
          console.log(error);
        }
      });
}