#List of all the inventory related logic

def parse_equip_action(item, player):
    action_text = item.data.action_details.split(" ")
    if action_text[1] == "armor_slot":
        if action_text[2] == "head":
            if(not player.armor.head == None):
                player.armor.head.inventory = player.inventory
                player.armor.head.is_equiped = False
                player.armor.head.save()
            player.armor.head = item
            player.armor.save()

            item.is_equiped = True
            item.save()

        elif action_text[2] == "torso":
            if(not player.armor.torso == None):
                player.armor.torso.inventory = player.inventory
                player.armor.torso.is_equiped = False
                player.armor.torso.save()
            player.armor.torso = item
            player.armor.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "legs":
            if(not player.armor.legs == None):
                player.armor.legs.inventory = player.inventory
                player.armor.legs.is_equiped = False
                player.armor.legs.save()
            player.armor.legs = item
            player.armor.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "feet":
            if(not player.armor.feet == None):
                player.armor.feet.inventory = player.inventory
                player.armor.feet.is_equiped = False
                player.armor.feet.save()
            player.armor.feet = item
            player.armor.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "hands":
            if(not player.armor.hands == None):
                player.armor.hands.inventory = player.inventory
                player.armor.hands.is_equiped = False
                player.armor.hands.save()
            player.armor.hands = item
            player.armor.save()

            item.is_equiped = True
            item.save()
    elif action_text[1] == "weapon_slot":
        if action_text[2] == "main":
            if(not player.main == None):
                player.main.inventory = player.inventory
                player.main.is_equiped = False
                player.main.save()
            player.main = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "offhand":
            if(not player.offhand == None):
                player.offhand.inventory = player.inventory
                player.offhand.is_equiped = False
                player.offhand.save()
            player.offhand = item
            player.save()

            item.is_equiped = True
            item.save()
    elif action_text[1] == "clothing_slot":
        if action_text[2] == "hat":
            if(not player.hat == None):
                player.hat.inventory = player.inventory
                player.hat.is_equiped = False
                player.hat.save()
            player.hat = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "spectacles":
            if(not player.spectacles == None):
                player.spectacles.inventory = player.inventory
                player.spectacles.is_equiped = False
                player.spectacles.save()
            player.spectacles = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "mask":
            if(not player.mask == None):
                player.mask.inventory = player.inventory
                player.mask.is_equiped = False
                player.mask.save()
            player.mask = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "shirt":
            if(not player.shirt == None):
                player.shirt.inventory = player.inventory
                player.shirt.is_equiped = False
                player.shirt.save()
            player.shirt = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "coat":
            if(not player.coat == None):
                player.coat.inventory = player.inventory
                player.coat.is_equiped = False
                player.coat.save()
            player.coat = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "gloves":
            if(not player.gloves == None):
                player.gloves.inventory = player.inventory
                player.gloves.is_equiped = False
                player.gloves.save()
            player.gloves = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "pants":
            if(not player.pants == None):
                player.pants.inventory = player.inventory
                player.pants.is_equiped = False
                player.pants.save()
            player.pants = item
            player.save()

            item.is_equiped = True
            item.save()
        elif action_text[2] == "socks":
            if(not player.socks == None):
                player.socks.inventory = player.inventory
                player.socks.is_equiped = False
                player.socks.save()
            player.socks = item
            player.save()

            item.is_equiped = True
            item.save()

def parse_unequip(item, player):
    action_details = item.data.action_details.split(" ")
    if action_details[1] == "weapon_slot":
        if action_details[2] == "main":
            player.main = None
            player.save()
        elif action_details[2] == "offhand":
            player.offhand = None
            player.save()
    elif(action_details[1] == "armor_slot"):
        if action_details[2] == "head":
            player.armor.head = None
            player.armor.save()
        elif action_details[2] == "torso":
            player.armor.torso = None
            player.armor.save()
        elif action_details[2] == "hands":
            player.armor.hands = None
            player.armor.save()
        elif action_details[2] == "legs":
            player.armor.legs = None
            player.armor.save()
        elif action_details[2] == "feet":
            player.armor.feet = None
            player.armor.save()
    elif(action_details[1] == "clothing_slot"):
        if action_details[2] == "hat":
            player.hat = None
            player.save()
        elif action_details[2] == "spectacles":
            player.spectacles = None
            player.save()
        elif action_details[2] == "mask":
            player.mask = None
            player.save()
        elif action_details[2] == "shirt":
            player.shirt = None
            player.save()
        elif action_details[2] == "coat":
            player.coat = None
            player.save()
        elif action_details[2] == "cloak":
            player.cloak = None
            player.save()
        elif action_details[2] == "gloves":
            player.gloves = None
            player.save()
        elif action_details[2] == "belt":
            player.belt = None
            player.save()
        elif action_details[2] == "pants":
            player.pants = None
            player.save()
        elif action_details[2] == "socks":
            player.socks = None
            player.save()

def parse_use_action(item, player):
    action_details = item.data.action_details.split(" ")
    if action_details[1] == "receive":
        if action_details[2] == "gold":
            player.inventory.gold += int(action_details[3])
            player.inventory.save()
            item.delete()