from django.urls import path
from game.views import *

urlpatterns = [
    path("start/", render_start, name='start'),
    path("map/", render_map, name="map"),
    path("map/search/", render_search, name="search"),
    path("movement/", movement_requests, name="movement"),
    path("player/inventory/", render_inventory, name="inventory"),  
    path("actions/", action_requests, name="actions"),
    path("actions/<str:menu>/<str:action_name>", search_requests, name="search actions"),
    path("dialogue/<int:npc_id>/", render_dialogue, name="dialogue"),
    path("dialogue/options/<int:npc_id>/", render_dialogue_options, name="dialogue options"),
    path("dialogue/actions/<int:npc_id>/", dialogue_requests, name="dialogue requests"),
    path("editor/create_map/", render_create_map, name="create map"),
    path("editor/map_painter/", render_map_painter, name="map painter")
    #path("map/upload", upload_map, name="upload map"),
]