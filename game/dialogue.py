from django.shortcuts import render, get_object_or_404, redirect
import random 
from .models import *

def higher_lower(index, max):
    if max == index:
        return random.randint(0,index-1)
    elif index == 0:
        return random.randint(index + 1, max)
    flip = random.randint(0,1)
    if flip:
        return random.randint(index + 1, max)
    else:
        return random.randint(0, index - 1)

def random_text_obj(queryset, player_param = None):
    max = len(queryset) - 1
    index = random.randint(0, max)
    if not player_param == None and queryset[index] == player_param:
        index = higher_lower(index, max)
    return queryset[index]
    
def greet_process(text):
    greetings = Greeting.objects.all()
    greeting = random_text_obj(greetings)
    text.current += "\n\t" + greeting.text
    text.save()

def farewell_process(text, player):
    farewells = Farewell.objects.all()
    farewell = random_text_obj(farewells, player.last_farewell)
    text.prev1 = text.current
    text.current = farewell.text
    player.last_farewell = farewell
    player.save()
    text.save()

def dd_response(text_object, filters, text, player, npc, player_param = None):
    #filters has to be a list
    text_responses = text_object.objects.filter(tags__contains = filters[0])
    if len(filters) == 1:
        if player_param:
            return random_text_obj(text_responses, player_param)
        else:
            return random_text_obj(text_responses)

    for filter in range(1, len(filters)):
        text_responses = text_responses.filter(tags__contains = filter)

    if player_param:
        return random_text_obj(text_responses, player_param)
    else:
        return random_text_obj(text_responses)

def display_dd(text_object, filters, text, player, npc, player_param = None):
    if player_param:
        dd = dd_response(text_object, filters, text, player, npc, player_param)
    else:
        dd = dd_response(text_object, filters, text, player, npc)
    text.current += "\n\t" + dd.text
    text.save()

def new_dialogue_options(options, option_type, player, npc):
    player.last_dialogue_actions = player.dialogue_actions
    player.dialogue_actions = options
    player.current_dialogue_options = option_type
    player.save()
    return redirect("dialogue options", npc.id)

def tell_joke(choice, text, player, npc):
    filter = choice.split(" ")[0]
    display_dd(Joke, [filter], text, player, npc)

def flatter_choice(option, text, player, npc):
    filter = option.split(" ")[0]
    display_dd(Joke, [filter], text, player, npc)

def flirt_choice(option, text, player, npc):
    filter = option.split(" ")[0]
    display_dd(Joke, [filter], text, player, npc)

def rumor_choice(option, text, player, npc):
    #need to create a rumor models
    display_dd(Joke, [option], text, player, npc)

def romance_choices(choice, text, player, npc):
    if choice == "flirt":
        options = "bad flirt,good flirt,back,root"
        option_type = "flirt"
        new_dialogue_options(options, option_type, player, npc)
    elif choice == "flatter":
        options = "bad flatter,good flatter,back,root"
        option_type = "flatter"
        new_dialogue_options(options, option_type, player, npc)

def friendly_choices(choice, text, player, npc):
    if choice == "favors":
        pass
    elif choice == "small talk":
        pass
    elif choice == "personal":
        pass
    elif choice == "cheer up":
        pass

def joke_options(player, npc):
    options = "bad joke,good joke,back,root"
    option_type = "jokes"
    new_dialogue_options(options, option_type, player, npc)   

def romance_options(player, npc):
    options = "flirt,flatter,back,root"
    option_type = "romance"
    new_dialogue_options(options, option_type, player, npc)   

def rumor_options(player, npc):
    options = "local,jobs,back,root"
    option_type = "rumor"
    new_dialogue_options(options, option_type, player, npc)   

def friendly_options(player, npc):
    options = "favors,small talk,personal,cheer up,back,root"
    option_type = "friendly"
    new_dialogue_options(options, option_type, player, npc) 

def talk_options(player):
    player.dialogue_actions = "joke,friendly,romance,rumors,root"
    player.save()

def option_processing(option, text, player, npc):
    if option == "root":
        player.current_dialogue_options = None
        player.dialogue_actions = None
        player.save()
        return redirect("dialogue", npc.id)
    elif option == "back":
        talk_options(player)
        player.current_dialogue_options = None
        player.save()
        return redirect("dialogue options", npc.id)
    elif player.current_dialogue_options == "jokes":
        tell_joke(option, text, player, npc)
    elif player.current_dialogue_options == "romance":
        romance_choices(option, text, player, npc)
    elif player.current_dialogue_options == "rumor":
        rumor_choice(option, text, player, npc)
    elif player.current_dialogue_options == "flirt":
        flirt_choice(option, text, player, npc)
    elif player.current_dialogue_options == "flatter":
        flatter_choice(option, text, player, npc)
    return redirect("dialogue options", npc.id)

def npc_appearance_text(npc):
    return f"npc is a {npc.race}"

def npc_start_text(npc, main_text):
    main_text.prev_1 = main_text.current
    main_text.current = npc_appearance_text(npc)
    main_text.save()

def dialogue_POST_handling(request, text, npc, player):
    action = request.POST["action"]
    if action == "greet":
        greet_process(text)
        return redirect("dialogue", npc.id)
    if not player.current_dialogue_options == None:
        print("entering options")
        return option_processing(action, text, player, npc)
    else:
        if action == "root":
            return redirect("dialogue", npc.id)
        elif action == "back":
            return redirect("dialogue options", npc.id)
        elif action == "trade":
            return redirect("trade")
        elif action == "talk":
            talk_options(player)
            return redirect("dialogue options", npc.id)
        elif action == "reset":
            text.current = ""
            text.save()
            npc.spoken_to_recently = False
            npc.save()
            player.current_dialogue_options = None
            player.dialogue_actions = None
            player.last_dialogue_actions = None
            player.save()
        elif action == "leave":
            npc.spoken_to_recently = False
            npc.save()
            farewell_process(text, player)
            player.current_dialogue_options = None
            player.dialogue_actions = None
            player.last_dialogue_actions = None
            player.save()
            return redirect("map")
        elif action == "joke":
            joke_options(player, npc)
            return redirect("dialogue options", npc.id)
        elif action == "romance":
            romance_options(player, npc)
            return redirect("dialogue options", npc.id)
        elif action == "rumors":
            rumor_options(player, npc)
            return redirect("dialogue options", npc.id)
    return redirect("dialogue", npc.id)

def npc_dialogue_data(npc, main_text, inserted = False, dict = {}):
    #dict = {}
    dict["npc_object"] = npc
    dict["main_text"] = main_text
    if(npc.name_known):
        dict["npc_name"] = npc.name
    else:
        dict["npc_name"] = npc.race
    if(not npc.spoken_to_recently):
        dict["action_list"] = ["greet", "trade", "talk", "leave"]
        npc.spoken_to_recently = True
        npc.save()
        npc_start_text(npc, main_text)
    else:
        dict["action_list"] = ["trade", "talk","reset","leave"]

    return dict
