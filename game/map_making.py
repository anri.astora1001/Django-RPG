from PIL import Image
from .models import *

def merge_row_to_map():
    map_img = Image.open("media/maps/map.jpg")

    current_row = Image.open("media/maps/current_row.jpg")

    new_map = Image.new("RGB", (map_img.width, map_img.height + current_row.height))
    new_map.paste(map_img, (0,0))
    new_map.paste(current_row, (0,map_img.height))
    new_map.save("media/maps/map.jpg")

def render_map_objects(node, map):
    x = node.current.name.split("_")[1].split(":")[0]
    y = node.current.name.split("_")[1].split(":")[1]
    tile_size = 32
    width = int(x) * tile_size
    height = int(y) * tile_size
    try:
        player = get_object_or_404(Player, id = 3)
        if(player.location.current.name == node.current.name):
                player_icon = Image.open("media/objects/player.png")
                #the image you need to paste on
                map.paste(player_icon, (width,height), player_icon)
                map.save("media/maps/game_screen.jpg")
                #current_tile = Image.alpha_composite(current_tile, player_icon)
    except:
        pass
    try:
        items = node.items
        for item in items:
            item_icon = Image.open(item.icon.path)
            map.paste(item_icon, (width,height), item_icon)
    except:
        pass

def all_map_objects(start_node):
    map = Image.open("media/maps/map.jpg")
    map.save("media/maps/game_screen.jpg")
    map = Image.open("media/maps/game_screen.jpg")

    render_map_objects(start_node, map)

    current_node = start_node
    direction = "east"
    first_counter = True
    while(True):
        if(direction == "east"):
            if(current_node.east == None):
                if(current_node.south == None):
                    #returns new map when it reaches the end
                    return   
                current_node = current_node.south
                render_map_objects(current_node, map)
                direction = "west"
                continue
            else:
                current_node = current_node.east
                render_map_objects(current_node, map)
                continue
        #does the same in the opposite direction
        elif(direction == "west"):
            if(current_node.west == None):
                if(current_node.south == None):
                    return 
                current_node = current_node.south
                render_map_objects(current_node, map)
                direction = "east"
                continue
            else:
                current_node = current_node.west
                render_map_objects(current_node, map)
                continue    



def compile_map_image(start_node):
    print("Entered map compiliation")
    if start_node.region.is_loaded:
        all_map_objects(start_node)
        return
    
    current_row = Image.open(start_node.current.tile.image.path)

    #render_objects(start_node, current_row)

    current_row.save("media/maps/current_row.jpg")

    current_node = start_node
    direction = "east"
    first_counter = True
    while(True):
        if(direction == "east"):
            if(current_node.east == None):
                if(current_node.south == None):
                    #returns new map when it reaches the end
                    merge_row_to_map()
                    start_node.region.is_loaded = True
                    start_node.region.save()
                    all_map_objects(start_node)
                    return 
                if(first_counter):
                    #logic to add first row to map_img
                    current_row = Image.open("media/maps/current_row.jpg")
                    current_row.save("media/maps/map.jpg")
                    first_counter = False
                else:
                    merge_row_to_map()    
                #logic move down row, reset current row and change directions
                current_node = current_node.south

                current_row = Image.open(current_node.current.tile.image.path)
                #render_objects(current_node, current_row)
                current_row.save("media/maps/current_row.jpg")
                direction = "west"
                continue
            else:
                #logic to add current_node to current_row
                current_node.merge_east(current_node.east.id)
                current_node = current_node.east
                continue
        #does the same in the opposite direction
        elif(direction == "west"):
            #print("entered west")
            if(current_node.west == None):
                if(current_node.south == None):
                    merge_row_to_map()
                    start_node.region.is_loaded = True
                    start_node.region.save()
                    all_map_objects(start_node)
                    return 
                else:
                    merge_row_to_map()
                #logic move down row, reset current row and change directions 
                current_node = current_node.south

                current_row = Image.open(current_node.current.tile.image.path)
                #render_objects(current_node, current_row)
                current_row.save("media/maps/current_row.jpg")
                direction = "east"
                continue
            else:
                #logic to add current_node to current_row
                current_node.merge_west(current_node.west.id)
                current_node = current_node.west
                continue   

def create_new_map(name, size):
    map_model = Map(name = name)
    map_model.save()

    size_parse = size.split("x")
    width = int(size_parse[0])
    height = int(size_parse[1])

    first = True
    #creates all the nodes and MapNodes
    print("=======================making nodes===========================")
    for x in range(width):
        print("------------------------------------------")
        for y in range(height):
            node_name = name+"_"+str(x)+":"+str(y)
            node = Node(name = node_name , description = name + " " + node_name, tile = get_object_or_404(Tile, id = 2))
            map_node = MapNode(current = node, region = map_model)
            node.save()
            map_node.save()
            print("node: " + node.name + " map_node: " + map_node.current.name)
            if(first):
                map_start = MapStart(region = map_model, start_node = map_node)
                map_start.save()
                first = False      
                print(map_start.start_node.current.name)
    print("==================================================")

    #create all the MapNodes
    print("========================Connections==========================")
    for x in range(width):
        print("------------------------------------------")
        for y in range(height):
            node_name = name+"_"+str(x)+":"+str(y)
            node = Node.objects.filter(name = node_name)
            node = node[0]
            map_node = MapNode.objects.filter(current = node)
            print("Query found: " + str(map_node))
            map_node = map_node[0]
            print(map_node.current.name)
            #left side
            if(x == 0):
                #left top corner
                if(y == 0):
                    node_name = name+"_"+str(x + 1)+":"+str(y)
                    print(node_name)
                    node = Node.objects.filter(name = node_name)
                    print(node[0])
                    node = node[0]
                    map_node_east = MapNode.objects.filter(current = node)
                    map_node_east = map_node_east[0]

                    map_node.east = map_node_east

                    node_name = name+"_"+str(x)+":"+str(y + 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_south = MapNode.objects.filter(current = node)
                    map_node_south = map_node_south[0]

                    map_node.south = map_node_south

                    map_node.save()
                    continue
                #left bottom corner
                elif(y == height - 1):
                    node_name = name+"_"+str(x + 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_east = MapNode.objects.filter(current = node)
                    map_node_east = map_node_east[0]

                    map_node.east = map_node_east

                    node_name = name+"_"+str(x)+":"+str(y - 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_north = MapNode.objects.filter(current = node)
                    map_node_north = map_node_north[0]

                    map_node.north = map_node_north

                    map_node.save()
                    continue
                #left side not corner
                else:
                    node_name = name+"_"+str(x + 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_east = MapNode.objects.filter(current = node)
                    map_node_east = map_node_east[0]

                    map_node.east = map_node_east

                    node_name = name+"_"+str(x)+":"+str(y - 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_north = MapNode.objects.filter(current = node)
                    map_node_north = map_node_north[0]

                    map_node.north = map_node_north

                    node_name = name+"_"+str(x)+":"+str(y + 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_south = MapNode.objects.filter(current = node)
                    map_node_south = map_node_south[0]

                    map_node.south = map_node_south

                    map_node.save()
                    continue
            #top side but top left corner already covered
            elif(y == 0):
                #right top corner
                if(x == width - 1):
                    node_name = name+"_"+str(x - 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_west = MapNode.objects.filter(current = node)
                    map_node_west = map_node_west[0]

                    map_node.west = map_node_west

                    node_name = name+"_"+str(x)+":"+str(y + 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_south = MapNode.objects.filter(current = node)
                    map_node_south = map_node_south[0]

                    map_node.south = map_node_south

                    map_node.save()
                    continue
                #not a top corner
                else:
                    node_name = name+"_"+str(x + 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_east = MapNode.objects.filter(current = node)
                    map_node_east = map_node_east[0]

                    map_node.east = map_node_east

                    node_name = name+"_"+str(x - 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_west = MapNode.objects.filter(current = node)
                    map_node_west = map_node_west[0]

                    map_node.west = map_node_west

                    node_name = name+"_"+str(x)+":"+str(y + 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_south = MapNode.objects.filter(current = node)
                    map_node_south = map_node_south[0]

                    map_node.south = map_node_south

                    map_node.save()
                    continue
            #bottom side but bottom left corner covered
            elif(y == height - 1):
                #bottom right corner
                if(x == width - 1):
                    node_name = name+"_"+str(x - 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_west = MapNode.objects.filter(current = node)
                    map_node_west = map_node_west[0]

                    map_node.west = map_node_west

                    node_name = name+"_"+str(x)+":"+str(y - 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_north = MapNode.objects.filter(current = node)
                    map_node_north = map_node_north[0]

                    map_node.north = map_node_north

                    map_node.save()
                    continue
                #bottom not corner
                else:
                    node_name = name+"_"+str(x + 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_east = MapNode.objects.filter(current = node)
                    map_node_east = map_node_east[0]

                    map_node.east = map_node_east

                    node_name = name+"_"+str(x - 1)+":"+str(y)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_west = MapNode.objects.filter(current = node)
                    map_node_west = map_node_west[0]

                    map_node.west = map_node_west

                    node_name = name+"_"+str(x)+":"+str(y - 1)
                    node = Node.objects.filter(name = node_name)
                    node = node[0]
                    map_node_north = MapNode.objects.filter(current = node)
                    map_node_north = map_node_north[0]

                    map_node.north = map_node_north

                    map_node.save()
                    continue
            #right side but both corners already covered
            elif(x == width - 1):
                node_name = name+"_"+str(x - 1)+":"+str(y)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_west = MapNode.objects.filter(current = node)
                map_node_west = map_node_west[0]

                map_node.west = map_node_west

                node_name = name+"_"+str(x)+":"+str(y - 1)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_north = MapNode.objects.filter(current = node)
                map_node_north = map_node_north[0]

                map_node.north = map_node_north

                node_name = name+"_"+str(x)+":"+str(y + 1)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_south = MapNode.objects.filter(current = node)
                map_node_south = map_node_south[0]

                map_node.south = map_node_south

                map_node.save()
                continue
            #not on any edge
            else:
                node_name = name+"_"+str(x + 1)+":"+str(y)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_east = MapNode.objects.filter(current = node)
                map_node_east = map_node_east[0]

                map_node.east = map_node_east

                node_name = name+"_"+str(x - 1)+":"+str(y)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_west = MapNode.objects.filter(current = node)
                map_node_west = map_node_west[0]

                map_node.west = map_node_west

                node_name = name+"_"+str(x)+":"+str(y - 1)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_north = MapNode.objects.filter(current = node)
                map_node_north = map_node_north[0]

                map_node.north = map_node_north

                node_name = name+"_"+str(x)+":"+str(y + 1)
                node = Node.objects.filter(name = node_name)
                node = node[0]
                map_node_south = MapNode.objects.filter(current = node)
                map_node_south = map_node_south[0]

                map_node.south = map_node_south

                map_node.save()
                continue            
    print("==================================================")