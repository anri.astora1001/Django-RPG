from django.shortcuts import render, get_object_or_404, redirect
from game.models import *
from game.dialogue import *
from .magic import *
from .map_making import *

def render_create_map(request):
    errors = []
    formatted = True
    if(request.method == "POST"):
        print(request.POST['name'])
        print(request.POST['size'])
        if(not len(request.POST['name'].split(" ")) == 1):
            errors.append("Only one word names accepted please use camel case")
            formatted = False
        if(Map.objects.filter(name= request.POST['name'] )):
            errors.append("Name already exists")
            formatted = False
        if(not len(request.POST['size'].split("x")) == 2 ):
            errors.append("map size expects WidthxHeight format EX: 24x24")  
            formatted = False
        else:     
            for num in request.POST['size'].split("x"):
                if(int(num) == num):
                    errors.append("Only intergers excepted for map size!")
                    formatted = False
                    break
            if(formatted):
                print("Formatted is True")
                name = request.POST['name']
                size = request.POST['size']

                create_new_map(name, size)
                
                return redirect("map painter")
    context = {
        "errors": errors
    }
    return render(request, "create_map.html", context)

def render_map_painter(request):
    errors = []
    map_bool = False
    formatted = True
    map_start = None
    last_entry_name = "map name"
    last_entry_location = "location"
    last_entry_tile = "tile name"
    if(request.method == "POST"):
        last_entry_name = request.POST['name']
        last_entry_location = request.POST['location']
        last_entry_tile = request.POST['tile']

        map_name = request.POST['name']
        tile_name = request.POST['tile']
        location = request.POST['location']

        #queries
        mapo = Map.objects.filter(name = map_name)
        tile = Tile.objects.filter(name = tile_name)
        if(not len(mapo) == 1):
            errors.append("Not a valid map name")
            formatted = False
        else:
            mapo = mapo[0]
            map_bool = True
            map_start = mapo.map_start.start_node
        if(not len(tile) == 1):
            errors.append("not a valid tile name")
            formatted = False
        if(not len(location.split(":")) == 2):
            errors.append("not a valid location format expects xCordinate:yCordinate Ex 5:1")
            formatted = False
        else:
            for num in location.split(":"):
                if(int(num) == num):
                    errors.append("Only intergers excepted for map location")
                    formatted = False
                    break  
            if(formatted):
                tile = tile[0]

                node_name = map_name + "_" + location

                node = Node.objects.filter(name = node_name)

                if(not len(node) == 1):
                    errors.append("something went wrong trying to retrieve the node. Node not found")
                    formatted = False
                if(formatted):
                    node = node[0]
                    node.tile = tile
                    node.save()
    if(map_bool):
        compile_map_image(map_start)

        context = {
            "errors": errors,
            "map_bool": map_bool,
            "map": map_start.region,
            "entry_name": last_entry_name,
            "entry_location": last_entry_location,
            "entry_tile": last_entry_tile,
        }
    else:
        context = {
                "errors": errors,
                "map_bool": map_bool,
                "map": "",
                "entry_name": last_entry_name,
                "entry_location": last_entry_location,
                "entry_tile": last_entry_tile,
            }

    return render(request, "map_painter.html", context)

# ===================================================================================

def movement_requests(request):
    if request.method == "POST":
        direction = request.POST['direction']
        if (direction == "down" or direction == "up" or direction == "right" or direction == "left"):
            #this ID is being hardcoded change it later
            player = get_object_or_404(Player, id = 3)
            player.movement(player, direction)

            return redirect("map")
    else:
        return redirect("map")

def action_requests(request):
    if request.method == "POST":
        action = request.POST['action']
        if action == "cast":
            #do the magic logic create a context and send it the map.html
            start_node = get_object_or_404(MapStart, id = 13)
            player = get_object_or_404(Player, id = 3)
            main_text = get_object_or_404(MainText, id = 1)
            actions, dialogue_actions = compile_actions(player.id)
            player = get_object_or_404(Player, id = 3)
            context = magic_context_data(player)
            context2 = {
                "map": start_node.region,
                "actions": actions,
                #"dialogue_bool": dialogue_bool,
                "dialogue_actions": dialogue_actions,
                "main_text": main_text
            }
            context = {**context, **context2}
            return render(request, "game.html", context)
        if len(action.split(" ")) > 1:
            if action.split(" ")[0] == "talk":
                return redirect("dialogue", action.split(" ")[1])
        elif action == "inventory":
            return redirect("inventory")
        elif action == "search":
            return redirect("search")
    else:
        return redirect("map")

def render_inventory(request):
    player = get_object_or_404(Player, id = 3)
    items = player.inventory.items.all()
    #print(items)
    context = {
        "player": player,
        "items": items,
    }
    return render(request, "inventory.html", context)


def search_requests(request, menu, action_name):
    player = get_object_or_404(Player, id = 3)
    if request.method == "POST":
        action = request.POST[f'{action_name}']
        #print(action)
        if action_name == "take":
            item = get_object_or_404(Item, id = action)
            player_inventory = get_object_or_404(Inventory, id = player.inventory.id)
            #item = item[0]
            item.inventory = player_inventory
            item.location = None
            item.save()
            print("took")
        elif action_name == "drop":
            item = get_object_or_404(Item, id = action)
            #item = item[0]
            item.inventory = None
            item.location = player.location
            item.save()
        elif action_name == "back":
            return redirect("map")
        elif action_name == "action":
            process_item_action(action, player)
        return redirect(menu)
    else:
        return redirect(menu)

def render_search(request):
    player = get_object_or_404(Player, id = 3)
    player_items = player.inventory.items.all()
    search_items = player.location.items.all()
    #print(items)
    context = {
        "player": player,
        "player_items": player_items,
        "search_items": search_items,
    }
    return render(request, "search.html", context)
    

def render_map(request):
    print("entering render")
    start_node = get_object_or_404(MapStart, id = 13)
    player = get_object_or_404(Player, id = 3)
    main_text = get_object_or_404(MainText, id = 1)
    compile_map_image(start_node.start_node)
    actions, dialogue_actions = compile_actions(player.id)
    
    dialogue_bool = True
    if(len(dialogue_actions) == 0):
        dialogue_bool = False
    context = {
        "map": start_node.region,
        "actions": actions,
        "dialogue_bool": dialogue_bool,
        "dialogue_actions": dialogue_actions,
        "main_text": main_text
    }
    return render(request, "game.html", context)

def render_start(request):
    if(request.method == "POST"):
        option = request.POST['option']
        if(option == "start"):
            return redirect('map')
    context = {
        "empty": "empty"
    }
    return render(request, "start_menu.html", context)

def dialogue_requests(request, npc_id):
    player = get_object_or_404(Player, id = 3)
    npc = get_object_or_404(NPC, id = npc_id)
    text = get_object_or_404(MainText, id = 1)
    if(request.method == "POST"):
        return dialogue_POST_handling(request, text, npc, player)
    else:
        return redirect("dialogue", npc.id)


def render_dialogue(request, npc_id):
    player = get_object_or_404(Player, id = 3)
    npc = get_object_or_404(NPC, id = npc_id)
    text = get_object_or_404(MainText, id = 1)

    context = npc_dialogue_data(npc, text)

    return render(request, "dialogue.html" , context)

def render_dialogue_options(request, npc_id):
    player = get_object_or_404(Player, id = 3)
    npc = get_object_or_404(NPC, id = npc_id)
    text = get_object_or_404(MainText, id = 1)

    context = {}
    if(npc.name_known):
        context["npc_name"] = npc.name
    else:
        context["npc_name"] = npc.race
    context["npc_object"] = npc
    context["main_text"] = text
    context["action_list"] = player.dialogue_actions.split(",")

    return render(request, "dialogue.html" , context)