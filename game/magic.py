from .models import *

def get_main_word(action, player):
    #player.known_power_words
    words = player.known_power_words.filter(tags_contains="starter")
    for word in words:
        if word.name == action:
            player.cast = action

def magic_POST_handling(request, text, npc, player):
    action = request.POST["action"]
    return

def magic_context_data(player):
    print("entered magic")
    magic_words = player.known_power_words.all()
    word = magic_words[0]
    print("first word: ")
    print(word)
    context = {
        "magic_words": magic_words,
    }

    return context