# Generated by Django 4.1.4 on 2023-01-03 21:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("game", "0044_alter_maintext_current_alter_maintext_prev_1"),
    ]

    operations = [
        migrations.AlterField(
            model_name="farewell",
            name="action_tags",
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name="farewell",
            name="tags",
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name="greeting",
            name="action_tags",
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name="greeting",
            name="tags",
            field=models.TextField(null=True),
        ),
    ]
