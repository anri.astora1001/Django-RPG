# Generated by Django 4.1.3 on 2023-01-03 16:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("game", "0042_race_npc_race"),
    ]

    operations = [
        migrations.AlterField(
            model_name="npc",
            name="location",
            field=models.ForeignKey(
                blank=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="npcs",
                to="game.mapnode",
            ),
        ),
    ]
