# Generated by Django 4.1.4 on 2022-12-28 23:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("game", "0032_gameitem_remove_item_description_remove_item_icon_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="ArmorSlot",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "feet",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="feet",
                        to="game.item",
                    ),
                ),
                (
                    "hands",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="hands",
                        to="game.item",
                    ),
                ),
                (
                    "head",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="head",
                        to="game.item",
                    ),
                ),
                (
                    "legs",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="legs",
                        to="game.item",
                    ),
                ),
                (
                    "torso",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="torso",
                        to="game.item",
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name="player",
            name="armor",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="player",
                to="game.armorslot",
            ),
        ),
    ]
