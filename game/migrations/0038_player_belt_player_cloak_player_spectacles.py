# Generated by Django 4.1.4 on 2022-12-30 00:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("game", "0037_player_coat_player_gloves_player_hat_player_mask_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="player",
            name="belt",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="player_belt",
                to="game.item",
            ),
        ),
        migrations.AddField(
            model_name="player",
            name="cloak",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="player_cloak",
                to="game.item",
            ),
        ),
        migrations.AddField(
            model_name="player",
            name="spectacles",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="player_spectacles",
                to="game.item",
            ),
        ),
    ]
