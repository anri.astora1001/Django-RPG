from django.forms import ModelForm
from game.models import Map


class UploadMapForm(ModelForm):
    class Meta:
        model = Map
        fields = (
            "image",
        )