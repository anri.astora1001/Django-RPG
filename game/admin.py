from django.contrib import admin
from game.models import *

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Tile)
class TileAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(Map)
class MapAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(Node)
class NodeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(MapNode)
class MapNodeAdmin(admin.ModelAdmin):
    model = MapNode
    list_display = (
        "get_node_name",
        "id",
    )
    @admin.display(description='Name', ordering='node__name')
    def get_node_name(self, obj):
        return obj.current.name

@admin.register(MapStart)
class MapStartAdmin(admin.ModelAdmin):
    list_display = (
        "region",
        "id",
    )

@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    list_display = (
        "data",
        "id",
    )

@admin.register(ArmorSlot)
class ArmorSlotAdmin(admin.ModelAdmin):
    list_display = (
        "inventory",
        "id",
    )

@admin.register(GameItem)
class GameItemAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(NPC)
class NPCAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )

@admin.register(PowerWord)
class PowerWordAdmin(admin.ModelAdmin):
    list_display = (
        "word",
        "id",
    )

@admin.register(Greeting)
class GreetingAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "tags",
        "action_tags",
        "id",
    )

@admin.register(Joke)
class JokeAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "tags",
        "action_tags",
        "id",
    )

@admin.register(Farewell)
class FarewellAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "tags",
        "action_tags",
        "id",
    )

@admin.register(Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = (
        "player",
        "gold",
        "id",
    )

@admin.register(MainText)
class MainTextAdmin(admin.ModelAdmin):
    list_display = (
        "id",
    )